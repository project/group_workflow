CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

The Group Workflow Integration module Allows you to integrate
the workflow module into the group module.

 * For a full description of the module, visit the project page:
   https://drupal.org/project/group_workflow

 * To submit bug reports and feature suggestions, or to track changes:
   https://drupal.org/project/issues/group_workflow


REQUIREMENTS
------------

This module very obviously requires:

 * Group
 * Gnode (sub-module of group)
 * Workflow


INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. Visit:
   https://drupal.org/documentation/install/modules-themes/modules-8
   for further information.


CONFIGURATION
-------------

The module has no menu or modifiable settings. There is no configuration.


MAINTAINERS
-----------

Current maintainers:
 * Jesse (jnicola) - https://www.drupal.org/user/224754
 